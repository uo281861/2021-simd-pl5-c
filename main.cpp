/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 * 
 * Exits exceptions:
 *	(0): Failed to load the source image
 *  (-1): Failed to load the background image
 * 	(-2): Different heights
 *  (-3): Different widths
 *  (-4): Failed to get the start time
 *  (-5): Failed to get the end time 
 */

#include <immintrin.h> //Including the intel intrinsic head
#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <time.h>

using namespace cimg_library;

// Data type for image components
typedef float data_t;
#define ITEMS_PER_PACKET (sizeof(__m128)/sizeof(data_t)) //package size under float size


const char* SOURCE_IMG      = "bailarina.bmp"; //Set the source image
const char* DESTINATION_IMG = "bailarina2.bmp"; //Set the name of destination image
const char* BACKGROUND_IMG = "background_V.bmp"; //Set the background image

const int REPETITIONS = 40; //Number of loops the program must do
const int TIMES_R = 10; //Number of loops to get time data the program must do

//Errors
const int EXIT_SOURCE = 0; //Failed to load the source image
const int EXIT_BACKGROUND = -1; //Failed to load the background image
const int EXIT_HEIGHT = -2; //Different heights
const int EXIT_WIDTH = -3; //Different widths
const int EXIT_START = -4; //Failed to get the start time
const int EXIT_END = -5; //Failed to get the end time

int main() {
	//Check the images exist
	FILE *file;
	if (!(file = fopen(SOURCE_IMG, "r"))) { //Source
		perror("Failed to load the source image");
		exit(EXIT_SOURCE);
	}
	if (!(file = fopen(BACKGROUND_IMG, "r"))) { //Background
		perror("Failed to load the background image");
		exit(EXIT_BACKGROUND);
	}

	// Open the source and background images
	CImg<data_t> srcImage(SOURCE_IMG);
	CImg<data_t> bgImage(BACKGROUND_IMG);

	data_t *pRbg, *pGbg, *pBbg; // Pointers to the R, G and B components of background
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components of source
	data_t *pRdest, *pGdest, *pBdest; // Pointers to the R, G and B components of destination image
	data_t *pDstImage; // Pointer to the new image pixels

	uint width, height; // Width and height of the source image
	uint widthB, heightB; // Width and height of the background image
	uint nComp; // Number of image components

	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
	// Common values for spectrum (number of image components):
	//  B&W images = 1
	//	Normal color images = 3 (RGB)
	//  Special color images = 4 (RGB and alpha/transparency channel)

	widthB = bgImage.width(); // Getting information from the background image
	heightB = bgImage.height();

	//Compares both hights of the images they are going to be used
	//If they are not both the same hight it throws an error
	if(height != heightB) {
		perror("Different heights on images");
		exit(EXIT_HEIGHT);
	}

	//Compares both widths of the images they are going to be used
	//If they are not both the same width it throws an error
	if(width != widthB) {
		perror("Different widths on images");
		exit(EXIT_WIDTH);
	}

	//Calculate vector size multiplying width by heigth
	#define VECTOR_SIZE  (width * height)

	//Vector size multiplied by float size all under package size (numbero of float in packages)
	int nPackages = (VECTOR_SIZE * sizeof(data_t) / sizeof(__m128));

	//If is not a exact number we need to add one more packet
	if ((VECTOR_SIZE * sizeof(data_t)) % sizeof(__m128) != 0){
		nPackages++;
	}

	struct timespec tStart, tEnd; //Initialize the variables need on the timer
    double dElapsedTimeS;

	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-6);
	}

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

	pRbg = bgImage.data(); // pRcomp points to the R component array
	pGbg = pRbg + height * width; // pGcomp points to the G component array
	pBbg = pGbg + height * width; // pBcomp points to B component array

	for(int j = 0; j < TIMES_R; j++){ //We iterate the code 10 times to get the execution times in a row

		//Start the timer
		if(clock_gettime(CLOCK_REALTIME, &tStart) == -1){
			perror("clock_gettime");
			exit(EXIT_START);
		}
		//Initialice packages for each R,G,B component of each image
		__m128 vSrcR, vSrcG, vSrcB;
		__m128 vDestR, vDestG, vDestB;
		__m128 vBgR, vBgG, vBgB;

        //Initialice auxiliary packages for  doing the operations in R, G, B destiny components
		__m128 v2, v255, vAuxR, vAuxG, vAuxB, vAuxR2, vAuxG2, vAuxB2, vAuxR3, vAuxG3, vAuxB3;

        //set a package to 2 and another to 255 for using them as constants in operations
		v2 = _mm_set1_ps(2);
		v255 = _mm_set1_ps(255);

        //inicialice R, G, B components of the destiny image to -1 to give them a value
		vDestR = _mm_set1_ps(-1);
		vDestG = _mm_set1_ps(-1);
		vDestB = _mm_set1_ps(-1);
		

		// Aplies the filter to the corresponding image
		for (uint i = 0; i < REPETITIONS; i++){
			for (int j = 0; j < nPackages; j++){
                //set every auxiliary package to 255
				vAuxR = _mm_set1_ps(255);
				vAuxG = _mm_set1_ps(255);
				vAuxB = _mm_set1_ps(255);
				
				vAuxR2 = _mm_set1_ps(255);
				vAuxG2 = _mm_set1_ps(255);
				vAuxB2 = _mm_set1_ps(255);

				vAuxR3 = _mm_set1_ps(255);
				vAuxG3 = _mm_set1_ps(255);
				vAuxB3 = _mm_set1_ps(255);
				
                //set to each R, G, B source package their corresponding value charged in the variables before
				vSrcR = _mm_loadu_ps(pRsrc + j * ITEMS_PER_PACKET);
				vSrcG = _mm_loadu_ps(pGsrc + j * ITEMS_PER_PACKET);
				vSrcB = _mm_loadu_ps(pBsrc + j * ITEMS_PER_PACKET);

                //set to each R, G, B baground package their corresponding value
				vBgR = _mm_loadu_ps(pRbg + j * ITEMS_PER_PACKET);
				vBgG = _mm_loadu_ps(pGbg + j * ITEMS_PER_PACKET);
				vBgB = _mm_loadu_ps(pBbg + j * ITEMS_PER_PACKET);

                //math operations to aply the algorithm to the R component
				vAuxR  = _mm_div_ps(vBgR, vAuxR);	//BgG entre 255
				vAuxR2 = _mm_sub_ps(vAuxR2, vBgR);	//255 - BgG
				vSrcR  = _mm_mul_ps(vSrcR, v2);		//vSrcG * 2
				vAuxR3 = _mm_div_ps(vSrcR, vAuxR3);	//vSrcG * 2 entre 255
				vDestR = _mm_mul_ps(vAuxR2, vAuxR3);//(255 - BgG) * (vSrcG * 2 entre 255)
				vDestR = _mm_add_ps(vDestR, vBgR);	//(255 - BgG) * (vSrcG * 2 entre 255) + BgG
				vDestR = _mm_mul_ps(vDestR, vAuxR);	//((255 - BgG) * (vSrcG * 2 entre 255) + BgG) * (BgG entre 255)

                //math operations to aply the algorithm to the G component
				vAuxG  = _mm_div_ps(vBgG, vAuxG);    
				vAuxG2 = _mm_sub_ps(vAuxG2, vBgG);   
				vSrcG  = _mm_mul_ps(vSrcG, v2);      
				vAuxG3 = _mm_div_ps(vSrcG, vAuxG3);  
				vDestG = _mm_mul_ps(vAuxG2, vAuxG3); 
				vDestG = _mm_add_ps(vDestG, vBgG);   
				vDestG = _mm_mul_ps(vDestG, vAuxG);  

                //math operations to aply the algorithm to the B component
				vAuxB  = _mm_div_ps(vBgB, vAuxB);
				vAuxB2 = _mm_sub_ps(vAuxB2, vBgB);
				vSrcB  = _mm_mul_ps(vSrcB, v2);
				vAuxB3 = _mm_div_ps(vSrcB, vAuxB3);
				vDestB = _mm_mul_ps(vAuxB2, vAuxB3);
				vDestB = _mm_add_ps(vDestB, vBgB);
				vDestB = _mm_mul_ps(vDestB, vAuxB);

                //set the resulting values of R, G, B components into the image variables making a cast to package
				//(__128) in them to do the pointer correectly
				*(__m128 *)(pRdest + j * ITEMS_PER_PACKET) = _mm_min_ps(vDestR, v255); 
				*(__m128 *)(pGdest + j * ITEMS_PER_PACKET) = _mm_min_ps(vDestG, v255);
				*(__m128 *)(pBdest + j * ITEMS_PER_PACKET) = _mm_min_ps(vDestB, v255);
			}
		}

		//Stop the timer
		if(clock_gettime(CLOCK_REALTIME, &tEnd) == -1){
			perror("clock_gettime");
			exit(EXIT_END);
		}
		dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
		dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;

		printf("Tiempo transcurrido: %f s.\n", dElapsedTimeS); //Print the timer
	}

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);
	dstImage.cut(0, 255); //Check rgb value is between 0 and 255

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG);

	// Display destination image
	dstImage.display();

	// Free memory
	_mm_free(pDstImage);

	return 0;
}
